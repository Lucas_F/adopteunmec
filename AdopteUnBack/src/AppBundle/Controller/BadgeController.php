<?php

namespace AppBundle\Controller;

use WideImage\WideImage;
use AppBundle\Entity\Badge;
use AppBundle\Form\BadgeType;
use Symfony\Component\VarDumper\VarDumper;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BadgeController extends Controller
{
    /**
     * @Route("/admin/createbadge", name="create_badge")
     */
    public function createBadgeAction(Request $request)
    {
        $error = '';
        try{
            $badge = new Badge();
            $form = $this->createForm(BadgeType::class, $badge);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $directory = $this->getParameter('badges_directory');
                $em = $this->getDoctrine()->getManager();
                $testName = $em->getRepository('AppBundle:Badge')->findBy(['name'=>$form['name']->getData()]);
                if(count($testName)>0){
                    $error = 'Badge '.$form['name']->getData().' already exist.';
                    return $this->render('@App/Badge/badge-create.html.twig', [
                        'form' => $form->createView(),
                        'error' => $error
                    ]);
                }
                $img = $form['img']->getData();
                $extension = $img->guessExtension();
                if($extension != 'png'){
                    $error = 'Image format different of PNG';
                    return $this->render('@App/Badge/badge-create.html.twig', [
                        'form' => $form->createView(),
                        'error' => $error
                    ]);
                }
                $badgeType = $request->get('badge-type');
                $sql = $badgeType ==="0" ? 'SELECT max(p.id) FROM AppBundle:Badge p WHERE p.id < 1000' : 'SELECT max(p.id) FROM AppBundle:Badge p';
                $idMax = intval($em->createQuery($sql)->getResult()[0][1])+1;
                $badge->setId($idMax);
                $imgname = strval($idMax).'.'.$extension;
                $img->move($directory, $imgname);
                $badge->setImg($imgname);
                $em->persist($badge);
                $em->flush();
                $this->redirectToRoute('create_badge');
            }
        }
        catch (\Exception $e){
            $error = $e->getMessage();
        }
        return $this->render('@App/Badge/badge-create.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
            'badges' => $this->getAllBadges()
        ]);
    }

    /**
     * @Route("/admin/formbadge", name="form_badge")
     */
    public function formBadgeAction(Request $request) {
        $badge = new Badge();
        
        $form = $this->createForm(BadgeType::class, $badge);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $directory = $this->getParameter('badges_directory');
            $manager = $this->getDoctrine()->getManager();

            $testName = $manager->getRepository('AppBundle:Badge')->findBy(['name'=>$form['name']->getData()]);

            $image = $form['img']->getData();
            $extension = $image->guessExtension();

            if (count($testName) > 0) {
                $error = 'Badge '.$form['name']->getData().' already exist.';
                return $this->generateResponse(500, $error);
            }            

            if ($extension != 'png') {
                $error = 'Image format different of PNG';
                return $this->generateResponse(500, $error);
            }

            $badgeType = $request->get('badge-type');
            $sql = $badgeType === "0" ? 'SELECT max(p.id) FROM AppBundle:Badge p WHERE p.id < 1000' : 'SELECT max(p.id) FROM AppBundle:Badge p';
            $idMax = intval($manager->createQuery($sql)->getResult()[0][1]) + 1;
            $imgname = strval($idMax).'.'.$extension;

            // Resize to 300x300 then update directory
            $image = WideImage::load($image->getRealPath());
            $image
                ->resizeDown(300, 300, 'inside')
                ->saveToFile($directory.'/'.$imgname)
            ;

            $badge->setId($idMax);
            $badge->setImg($imgname);
            
            $manager->persist($badge);
            $manager->flush();

            return $this->generateResponse(200, $this->generateNewBadgeDiv($imgname, $badge->getName()));
        }

        return $this->render('@App/Badge/badge-form.html.twig', ['form_badge'=>$form->createView()]);
    }

    /**
     * @Route("/admin/deletebadge/{idBadge}", name="delete_badge")
     */
    public function deleteBadgeAction(Request $request, $idBadge)
    {
        if($request->getMethod()=='DELETE'){
            try{
                $em = $this->getDoctrine()->getManager();
                $badge = $em->getRepository('AppBundle:Badge')->find($idBadge);
                $em->remove($badge);
                $em->flush();
            }
            catch (\Exception $e){
                return $this->generateResponse(500, $e->getMessage());
            }
            return $this->generateResponse(200, 'deleted');
        }
        return $this->generateResponse(405, 'Method not allowed.');
    }

    private function generateResponse($status, $response){
        return new JsonResponse([
            'status' => $status,
            'response' => $response
        ]);
    }

    private function generateNewBadgeDiv($img, $name)
    {
        $html = '<div class="badge-container">
                <img src="/assets/badges/'.$img.'" alt="img" style="height: 75px; width: auto;"/>
                <p style="text-align: center">'.$name.'</p>
                </div>';
        return $html;
    }

    private function getAllBadges()
    {
        $em = $this->getDoctrine()->getManager();
        $badges = $em->getRepository('AppBundle:Badge')->findAll();
        $badges_array = [];
        foreach ($badges as $badge){
            array_push($badges_array, [
               'name' => $badge->getName(),
               'img' => '/assets/badges/'.$badge->getImg()
            ]);
        }
        return $badges_array;
    }

}