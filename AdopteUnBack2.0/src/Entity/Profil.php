<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfilRepository")
 */
class Profil
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="json")
     */
    private $links = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pdfCV;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $linkCV;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $video;

    /**
     * @ORM\Column(type="boolean")
     */
    private $adopted;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $promo;

    /**
     * @ORM\Column(type="text")
     */
    private $intro;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $img;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProfilBadge", mappedBy="profilId", orphanRemoval=true)
     */
    private $profilBadges;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="profilId", orphanRemoval=true)
     */
    private $projects;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="profil")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="profil", orphanRemoval=true)
     */
    private $comments;


    private $score;

    public function __construct()
    {
        $this->profilBadges = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getLinks(): ?array
    {
        return $this->links;
    }

    public function setLinks(array $links): self
    {
        $this->links = $links;

        return $this;
    }

    public function getPdfCV(): ?string
    {
        return $this->pdfCV;
    }

    public function setPdfCV(string $pdfCV): self
    {
        $this->pdfCV = $pdfCV;

        return $this;
    }

    public function getLinkCV(): ?string
    {
        return $this->linkCV;
    }

    public function setLinkCV(string $linkCV): self
    {
        $this->linkCV = $linkCV;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getAdopted(): ?bool
    {
        return $this->adopted;
    }

    public function setAdopted(bool $adopted): self
    {
        $this->adopted = $adopted;

        return $this;
    }

    public function getPromo(): ?string
    {
        return $this->promo;
    }

    public function setPromo(string $promo): self
    {
        $this->promo = $promo;

        return $this;
    }

    public function getIntro(): ?string
    {
        return $this->intro;
    }

    public function setIntro(string $intro): self
    {
        $this->intro = $intro;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection|ProfilBadge[]
     */
    public function getProfilBadges(): Collection
    {
        return $this->profilBadges;
    }

    public function addProfilBadge(ProfilBadge $profilBadge): self
    {
        if (!$this->profilBadges->contains($profilBadge)) {
            $this->profilBadges[] = $profilBadge;
            $profilBadge->setProfilId($this);
        }

        return $this;
    }

    public function removeProfilBadge(ProfilBadge $profilBadge): self
    {
        if ($this->profilBadges->contains($profilBadge)) {
            $this->profilBadges->removeElement($profilBadge);
            // set the owning side to null (unless already changed)
            if ($profilBadge->getProfilId() === $this) {
                $profilBadge->setProfilId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setProfilId($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getProfilId() === $this) {
                $project->setProfilId(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setProfil($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getProfil() === $this) {
                $comment->setProfil(null);
            }
        }

        return $this;
    }

    public function jsonSerialize()
    {
        $array = array(
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName'=> $this->lastName,
            'contact' => [
                'phone' => $this->phone,
                'birthday' => $this->birthday->format('d/m/Y'),
                'city' => $this->city
            ],
            'socials' => $this->links,
            'medias' => [
                'video' => $this->video,
                'pdfCv' => $this->pdfCV,
                'linkCv' => $this->linkCV,
            ],
            'adopted' => $this->adopted,
            'promo' => $this->promo,
            'intro' => $this->intro,
            'img' => $this->img,
        );
        if($this->user != null)
            $array['contact']['mail'] = $this->user->getEMail();
        return $array;
    }

    public function setScore($score){
        $this->$score = $score;
    }
    public function getScore(){
        return $this->score;
    }


    public function __toString(){
        return $this->firstName;
    }

}
