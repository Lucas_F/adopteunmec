socials = ['fb', 'lkdn', 'twtr', 'insta', 'github', 'gitlab', 'so', 'other']
keys = ['id', 'lastName', 'firstName', 'birthday', 'phone', 'fb', 'lkdn', 'twtr', 'insta', 'github', 'gitlab', 'so', 'other', 'pdfCv', 'linkCv', 'video', 'adopted', 'promo', 'intro', 'city', 'img']
profil = [
    [1, 'Romain', 'Vrébos', '2000-09-23 00:00:00', '0666219841', '', 'https://www.linkedin.com/in/romain-vr%C3%A9bos-68ba95174/', '', '', 'https://github.com/RVrebos', 'https://gitlab.com/airbound', '', '{}', '1.pdf', '', 'video.mp4', 1, 'A2 Dev', None, None, None],
    [2, 'Alexis', 'Combes', '1994-11-16 00:00:00', '0645793588', '', 'https://www.linkedin.com/in/alexis-combes-297042177/', '', '', '', '', '', '{}', '2.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [3, 'Augustin', 'HAVAS', '1999-01-03 00:00:00', '0782542767', '', 'https://www.linkedin.com/in/augustin-havas-24a044177/', '', '', '', '', '', '{}', None, '', 'video.mp4', 1, 'A2 Dev', None, None, None],
    [4, 'Benjamin', 'Helou', '2000-12-03 00:00:00', '0622787954', '', 'https://www.linkedin.com/in/benjamin-helou-7650b7176/', '', '', 'https://github.com/BenjaminHelou', 'https://gitlab.com/BHelou', '', '{}', '4.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [5, 'Nathanael', 'Elisabeth', '2000-10-13 00:00:00', '0673958772', '', 'https://www.linkedin.com/in/nathana%C3%ABl-elisabeth-a4404a177/', '', '', '', '', '', '{}', '5.pdf', '', 'video.mp4', 1, 'A2 Dev', None, None, None],
    [6, 'Lucas', 'Folliot', '2000-08-02 00:00:00', '0625654143', 'https://www.facebook.com/lucas.folliot', 'https://www.linkedin.com/in/lucas-folliot-53824a175/', '', '', '', 'https://www.gitlab.com/Lucas_F', '', '{}', '6.pdf', '', 'video.mp4', 1, 'A2 Dev', None, None, None],
    [7, 'Jules', 'Masson', '2000-04-16 00:00:00', '0761792593', '', 'https://www.linkedin.com/in/jules-masson-6a0778176/', '', '', '', 'https://www.gitlab.com/JulesMasson', '', '{}', '7.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [8, 'Robin', 'Laillier', '1999-05-30 00:00:00', '0652910191', '', 'https://www.linkedin.com/in/robin-laillier-77b089176/', '', '', '', 'https://gitlab.com/RobinLaillier', '', '{}', '8.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [9, 'Martin', 'Hardant', '1998-12-25 00:00:00', '0626810500', '', '', '', '', '', '', '', '{}', None, '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [10, 'Hélène', 'Wattiaux', '1995-10-07 00:00:00', '0760000710', '', 'https://www.linkedin.com/in/hélène-wattiaux-96470b121/', 'https://twitter.com/Hellycia_', '', '', '', '', '{}', '10.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [11, 'Nils', 'Lecointe', '1996-01-16 00:00:00', '0668350914', '', 'https://www.linkedin.com/in/nils-lecointe-13a134113/', '', '', '', '', '', '{}', '11.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [12, 'Kevin', 'Combes', '1999-09-19 00:00:00', '0783391195', '', 'https://www.linkedin.com/in/kevin-combes-077b18175/', '', '', '', '', '', '{}', '12.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [13, 'Louise', 'Louvieaux', '1996-06-26 00:00:00', '0649695808', '', 'https://www.linkedin.com/in/louise-louvieaux-a81967175/', '', '', '', '', '', '{}', '13.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [14, 'Nicolas', 'Levaufre', '1988-12-07 00:00:00', '0671124203', 'https://www.facebook.com/nicolas.levaufre.9', 'https://www.linkedin.com/in/nicolas-levaufre-594363161/', 'https://twitter.com/NicolasLevaufre', '', 'https://github.com/NicolasLevaufre', 'https://gitlab.com/NLevaufre', 'https://stackoverflow.com/users/10811124/nicolas-l', '{}', '14.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [15, 'Arthur', 'Foutrel', '1996-08-07 00:00:00', '0671120436', 'https://www.facebook.com/arthur.foutrel', 'https://www.linkedin.com/in/arthur-foutrel-41a779175/', '', '', '', 'https://gitlab.com/Aeddan', '', '{}', '15.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [16, 'Carl', 'Delacour', '1992-09-24 00:00:00', '0699817784', '', 'https://fr.linkedin.com/carl-delacour-1447087a', '', '', '', '', '', '{}', '16.pdf', '', 'video.mp4', 1, 'A2 Dev', None, None, None],
    [17, 'Jean-Michel', 'PUMONT', '1986-01-11 00:00:00', '0668674565', '', '', '', '', '', 'https://gitlab.com/Jmeuh', '', '{}', '17.pdf', '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [18, 'Lucas', 'LABIGNE', '1996-01-23 00:00:00', '0603889608', 'https://www.facebook.com/lucas.labigne', 'https://www.linkedin.com/in/lucas-labigne-33617b165/', 'https://twitter.com/Lulu_Lab', 'https://www.instagram.com/lucas__labigne/?hl=fr', 'https://github.com/LucasLBGN', 'https://gitlab.com/LucasLBGN', '', '{}', None, '', 'https://www.youtube.com/embed/OqweMDSvAZE', 1, 'A2 Dev', None, None, None],
    [19, 'Maxime', 'Marie', '1989-06-02 00:00:00', '0626514342', '', '', '', '', '', '', '', '{}', None, '', 'video.mp4', 0, 'A2 Dev', None, None, None],
    [20, 'Damien', 'Hebert', '1998-10-08 00:00:00', '0606060606', '', 'https://www.linkedin.com/in/damien-hebert/', '', '', '', '', '', '{}', None, '', 'video.mp4', 1, 'A2 Dev', None, None, None],
    [21, 'Alexandre', 'Collin', '1997-10-11 00:00:00', '0606060606', 'https://www.facebook.com/alexandre.collin.18?ref=bookmarks', 'https://www.linkedin.com/in/alexandre-collin-3b7394118/', '', '', 'https://github.com/Ithunderbird', 'https://gitlab.com/Ithunderbird', '', '{}', '21.pdf', '', 'video.mp4', 1, 'A2 Dev', None, None, None]
]

profilsEnd = []

for p in profil:
    profilEnd = []
    link = {}
    c = 0
    for k in range(0,len(p)):
        if( k < 5 or k > 12):
            profilEnd.append(p[k])
        else:
            link[keys[k]] = p[k]
        if(k == 12):
            profilEnd.append(link)
    profilsEnd.append(profilEnd)
print(profilsEnd)

